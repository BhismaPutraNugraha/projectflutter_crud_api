import 'dart:ui';
import 'package:flutter/material.dart';

class Profil extends StatelessWidget {
  @override
  // ignore: override_on_non_overriding_member
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profil'),
      ),
      body: MyProfil(),
    );
  }
}

class MyProfil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Hero(
            tag: 'Profile',
            child: CircleAvatar(
              backgroundImage: AssetImage('././assets/Bhisma.jpg'),
              radius: 80,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Profile Developer',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: 20,
            width: 180,
            child: Divider(
              color: Colors.black,
              thickness: 6,
            ),
          ),
          Card(
            margin: EdgeInsets.only(top: 20, left: 10, right: 10),
            color: Colors.cyan,
            child: ListTile(
              leading: Icon(Icons.account_box_sharp),
              title: Text('I Made Bhisma Putra Nugraha'),
              contentPadding: EdgeInsets.all(8),
            ),
          ),
          Card(
            margin: EdgeInsets.only(top: 20, left: 10, right: 10),
            color: Colors.cyan,
            child: ListTile(
              leading: Icon(Icons.email),
              title: Text('bhismaputran04@gmail.com'),
              contentPadding: EdgeInsets.all(8),
            ),
          ),
          Card(
            margin: EdgeInsets.only(top: 20, left: 10, right: 10),
            color: Colors.cyan,
            child: ListTile(
              leading: Icon(Icons.assignment_rounded),
              title: Text('1915051063'),
              contentPadding: EdgeInsets.all(8),
            ),
          ),
          Card(
            margin: EdgeInsets.only(top: 20, left: 10, right: 10),
            color: Colors.cyan,
            child: ListTile(
              leading: Icon(Icons.home_work_sharp),
              title: Text('PTI 4B'),
              contentPadding: EdgeInsets.all(8),
            ),
          ),
        ],
      ),
    );
  }
}
